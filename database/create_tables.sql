CREATE SCHEMA innomachina;

CREATE TABLE innomachina.address (
	id                   SERIAL,
	building             varchar(10)  ,
	street               varchar(150)  NOT NULL,
	city                 varchar(120)  NOT NULL,
	country_code         char(2)  NOT NULL,
	CONSTRAINT pk_address PRIMARY KEY ( id )
 );

CREATE TABLE innomachina.gps (
	id                   SERIAL,
	latitude             float8  NOT NULL,
	longitude            float8  NOT NULL,
	CONSTRAINT pk_gps PRIMARY KEY ( id )
 );

CREATE TABLE innomachina.model (
	id                   SERIAL,
	name                 varchar(100)  NOT NULL UNIQUE,
	plug_shape           integer  NOT NULL,
	CONSTRAINT pk_model PRIMARY KEY ( id )
 );

CREATE TABLE innomachina.part (
	id                   SERIAL,
	p_type               varchar(50)  NOT NULL,
	p_size               integer  NOT NULL,
	cost                 money DEFAULT 0 NOT NULL,
	CONSTRAINT pk_part PRIMARY KEY ( id )
 );

CREATE TABLE innomachina.parts_compatibility (
	id                   SERIAL,
	car_model            integer  NOT NULL,
	part                 integer  NOT NULL,
	CONSTRAINT pk_parts_compatibility PRIMARY KEY ( id ),
	CONSTRAINT fk_parts_compatibility FOREIGN KEY ( part ) REFERENCES innomachina.part( id )    ,
	CONSTRAINT fk_parts_compatibility_0 FOREIGN KEY ( car_model ) REFERENCES innomachina.model( id )
 );

CREATE TABLE innomachina.provider (
	id                   SERIAL,
	name                 varchar(100)  NOT NULL,
	address              integer  ,
	phone                varchar(12)  ,
	loc                  integer  ,
	CONSTRAINT pk_provider PRIMARY KEY ( id ),
	CONSTRAINT fk_provider FOREIGN KEY ( address ) REFERENCES innomachina.address( id )    ,
	CONSTRAINT fk_provider_0 FOREIGN KEY ( loc ) REFERENCES innomachina.gps( id )
 );

CREATE TABLE innomachina.station (
	id                   SERIAL,
	loc                  integer  ,
	price                money  ,
	charging_time        integer  ,
	CONSTRAINT pk_station PRIMARY KEY ( id ),
	CONSTRAINT fk_station FOREIGN KEY ( loc ) REFERENCES innomachina.gps( id )
 );

CREATE TABLE innomachina.customer (
	id                   SERIAL,
	balance              integer DEFAULT 0 NOT NULL,
	email                varchar  ,
	fullname             varchar(100)  NOT NULL,
	username             varchar(100)  NOT NULL,
	phone                varchar(14)  ,
	address              integer  ,
	CONSTRAINT pk_customer PRIMARY KEY ( id ),
	CONSTRAINT fk_user FOREIGN KEY ( address ) REFERENCES innomachina.address( id )
 );

CREATE TABLE innomachina.workshop (
	id                   SERIAL,
	address              integer  NOT NULL,
	loc                  integer  NOT NULL,
	working_hours_open   time DEFAULT '09:00' NOT NULL,
	working_hours_close  time DEFAULT '18:00' NOT NULL,
	CONSTRAINT pk_workshop PRIMARY KEY ( id ),
	CONSTRAINT fk_workshop FOREIGN KEY ( loc ) REFERENCES innomachina.gps( id )    ,
	CONSTRAINT fk_workshop_0 FOREIGN KEY ( address ) REFERENCES innomachina.address( id )
 );

CREATE TABLE innomachina.car (
	id                   SERIAL,
	loc                  integer  ,
	model                integer  NOT NULL,
	rate                 float  NOT NULL DEFAULT 0.0,
	plate                varchar(8)  NOT NULL,
	color                varchar  NOT NULL,
	CONSTRAINT pk_car PRIMARY KEY ( id ),
	CONSTRAINT fk_car FOREIGN KEY ( loc ) REFERENCES innomachina.gps( id )    ,
	CONSTRAINT fk_car_0 FOREIGN KEY ( model ) REFERENCES innomachina.model( id )
 );

CREATE TABLE innomachina.catalogue (
	id                   SERIAL,
	provider             integer  NOT NULL,
	part                 integer  NOT NULL,
	status               char(1) DEFAULT 'A' NOT NULL,
	CONSTRAINT pk_catalogue PRIMARY KEY ( id ),
	CONSTRAINT fk_catalogue FOREIGN KEY ( provider ) REFERENCES innomachina.provider( id )    ,
	CONSTRAINT fk_catalogue_0 FOREIGN KEY ( part ) REFERENCES innomachina.part( id )
 );

CREATE TABLE innomachina.plug (
	id                   SERIAL,
	station              integer  NOT NULL,
	shape                varchar(2) DEFAULT 'B' NOT NULL,
	socket_size          integer  NOT NULL,
	CONSTRAINT pk_plug PRIMARY KEY ( id ),
	CONSTRAINT fk_plug FOREIGN KEY ( station ) REFERENCES innomachina.station( id )
 );

CREATE TABLE innomachina.rent (
	id                   SERIAL,
	customer             integer  NOT NULL,
	car                  integer  NOT NULL,
	arrival_from         integer  NOT NULL,
	arrived_to           integer  ,
	distance             float8 DEFAULT 0.0 NOT NULL,
	status               char(1) DEFAULT 'O' NOT NULL,
	order_stamp          timestamp DEFAULT current_timestamp NOT NULL,
	delivered_stamp      timestamp  ,
	finished_stamp       timestamp  ,
	CONSTRAINT pk_rent PRIMARY KEY ( id ),
	CONSTRAINT fk_rent FOREIGN KEY ( customer ) REFERENCES innomachina.customer( id )    ,
	CONSTRAINT fk_rent_0 FOREIGN KEY ( car ) REFERENCES innomachina.car( id )    ,
	CONSTRAINT fk_rent_1 FOREIGN KEY ( arrival_from ) REFERENCES innomachina.gps( id )    ,
	CONSTRAINT fk_rent_2 FOREIGN KEY ( arrived_to ) REFERENCES innomachina.gps( id )
 );

CREATE TABLE innomachina.repairs (
	id                   SERIAL,
	car                  integer  NOT NULL,
	workshop             integer  NOT NULL,
	repair_start         timestamp DEFAULT current_timestamp NOT NULL,
	repair_stop          timestamp  ,
	labour               integer DEFAULT 0 NOT NULL,
	CONSTRAINT pk_repairs PRIMARY KEY ( id ),
	CONSTRAINT fk_repairs FOREIGN KEY ( car ) REFERENCES innomachina.car( id )    ,
	CONSTRAINT fk_repairs_2 FOREIGN KEY ( workshop ) REFERENCES innomachina.workshop( id )
 );

CREATE TABLE innomachina.repair_used_parts (
 	id                   SERIAL,
 	repair               integer  NOT NULL,
 	part                 integer  NOT NULL,
 	CONSTRAINT pk_repairs_used_parts PRIMARY KEY ( id ),
 	CONSTRAINT fk_repairs_used_parts FOREIGN KEY ( repair ) REFERENCES innomachina.repairs( id )    ,
 	CONSTRAINT fk_repairs_used_parts_0 FOREIGN KEY ( part ) REFERENCES innomachina.part( id )
);

CREATE TABLE innomachina.shipment (
	id                   SERIAL,
	workshop             integer  NOT NULL,
	provider             integer  NOT NULL,
	part                 integer  NOT NULL,
	p_count              integer DEFAULT 0 NOT NULL,
	status               char(1) DEFAULT 'O' NOT NULL,
	sent_timestamp       timestamp DEFAULT current_timestamp NOT NULL,
	delivered_timestamp  timestamp  NOT NULL,
	CONSTRAINT pk_shipment PRIMARY KEY ( id ),
	CONSTRAINT fk_shipment FOREIGN KEY ( workshop ) REFERENCES innomachina.workshop( id )    ,
	CONSTRAINT fk_shipment_0 FOREIGN KEY ( provider ) REFERENCES innomachina.provider( id )    ,
	CONSTRAINT fk_shipment_1 FOREIGN KEY ( part ) REFERENCES innomachina.part( id )
 );

CREATE TABLE innomachina.transactions (
	id                   SERIAL,
	customer             integer  NOT NULL,
	rent                 integer  NOT NULL,
	amount               money DEFAULT 0 NOT NULL,
	residue              money DEFAULT 0 NOT NULL,
	stamp                timestamp DEFAULT current_timestamp NOT NULL,
	CONSTRAINT pk_transactions PRIMARY KEY ( id ),
	CONSTRAINT fk_transactions FOREIGN KEY ( customer ) REFERENCES innomachina.customer( id )    ,
	CONSTRAINT fk_transactions_0 FOREIGN KEY ( rent ) REFERENCES innomachina.rent( id )
 );

CREATE TABLE innomachina.history (
	id                   SERIAL,
	plug                 integer  NOT NULL,
	car                  integer NOT NULL,
	customer             integer NOT NULL,
	charging_started     timestamp DEFAULT current_timestamp NOT NULL,
	charging_finished    timestamp  ,
	CONSTRAINT pk_history PRIMARY KEY ( id ),
	CONSTRAINT fk_history FOREIGN KEY ( plug ) REFERENCES innomachina.plug( id )
	CONSTRAINT fk_history FOREIGN KEY ( car ) REFERENCES innomachina.car( id )
	CONSTRAINT fk_history FOREIGN KEY ( cusomer ) REFERENCES innomachina.cusomer( id )
);

CREATE INDEX idx_transactions ON innomachina.transactions ( customer );
CREATE INDEX idx_transactions_0 ON innomachina.transactions ( rent );

CREATE INDEX idx_shipment ON innomachina.shipment ( workshop );
CREATE INDEX idx_shipment_0 ON innomachina.shipment ( provider );
CREATE INDEX idx_shipment_1 ON innomachina.shipment ( part );

CREATE INDEX idx_repairs ON innomachina.repairs ( car );
CREATE INDEX idx_repairs_1 ON innomachina.repairs ( workshop );

CREATE INDEX idx_rent ON innomachina.rent ( customer );
CREATE INDEX idx_rent_0 ON innomachina.rent ( car );
CREATE INDEX idx_rent_1 ON innomachina.rent ( arrival_from );
CREATE INDEX idx_rent_2 ON innomachina.rent ( arrived_to );

CREATE INDEX idx_plug ON innomachina.plug ( station );

CREATE INDEX idx_catalogue ON innomachina.catalogue ( provider );
CREATE INDEX idx_catalogue_0 ON innomachina.catalogue ( part );

CREATE INDEX idx_car ON innomachina.car ( loc );
CREATE INDEX idx_car_0 ON innomachina.car ( model );

CREATE INDEX idx_workshop ON innomachina.workshop ( loc );
CREATE INDEX idx_workshop_0 ON innomachina.workshop ( address );

CREATE INDEX idx_user ON innomachina.customer ( address );

CREATE INDEX idx_station ON innomachina.station ( loc );

CREATE INDEX idx_provider ON innomachina.provider ( address );
CREATE INDEX idx_provider_0 ON innomachina.provider ( loc );

CREATE INDEX idx_parts_compatibility ON innomachina.parts_compatibility ( part );
CREATE INDEX idx_parts_compatibility_0 ON innomachina.parts_compatibility ( car_model );

CREATE INDEX idx_history ON innomachina.history ( plug );

CREATE INDEX idx_repairs_used_parts ON innomachina.repair_used_parts ( repair );
CREATE INDEX idx_repairs_used_parts_0 ON innomachina.repair_used_parts ( part );
