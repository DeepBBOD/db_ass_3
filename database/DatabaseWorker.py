#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import dateutil.relativedelta
import psycopg2
from random import randrange, randint
from datetime import timedelta, datetime


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


class Worker:
    def __init__(self, user, dbname, password, host, port=5432, schema='innomachina'):
        self.host = host
        self.user = user
        self.dbname = dbname
        self.password = password
        self.port = port
        self.schema = schema

        self.conn = None
        self.connect()

        self.execute('SET search_path TO {};'.format(schema), commit=True)

    def connect(self):
        self.conn = psycopg2.connect(
            dbname=self.dbname,
            user=self.user,
            host=self.host,
            password=self.password,
            port=self.port
        )

    def execute(self, request, commit=False, fetchall=False):
        with self.conn.cursor() as cursor:
            cursor.execute(request)

            if commit:
                self.conn.commit()
                return True
            elif fetchall:
                return cursor.fetchall()
            else:
                return cursor.fetchone()

    def create_tables(self, sql_file):
        with open(sql_file, 'r') as file:
            sql = file.read()
            return self.execute(sql, commit=True)

    def rollback(self):
        self.conn.rollback()

    def get_random_car(self):
        car_data = self.execute("""SELECT id FROM car;""", fetchall=True)
        car_ids = [x[0] for x in car_data]
        return car_ids[randint(0, len(car_ids) - 1)]

    def get_random_user(self):
        user_data = self.execute("""SELECT id FROM customer;""", fetchall=True)
        user_ids = [x[0] for x in user_data]
        return user_ids[randint(0, len(user_ids))]

    def add_rent_car(self, user_id):
        date_start = datetime.strptime('2018-05-22', '%Y-%m-%d')
        date_finished = random_date(start=date_start, end=datetime.now())
        car_id = self.get_random_car()

        res = self.execute("""INSERT INTO rent (customer,car,arrival_from,arrived_to,
                                            distance,status,order_stamp,delivered_stamp,finished_stamp)
                                            VALUES
                                            ({},{},2,3,0.149726056070328,'a',
                                            '{}','{}','{}') RETURNING id;""".format(user_id,
                                                                                    car_id,
                                                                                    date_start,
                                                                                    date_start,
                                                                                    date_finished),
                             commit=True)

        if res:
            inserted_rent_id = worker.execute("""SELECT currval(pg_get_serial_sequence('rent','id'));""")[0]

            self.execute("""INSERT INTO transactions (customer,rent,amount,residue)
                              VALUES ({},{},'{}','{}');""".format(user_id, inserted_rent_id,
                                                                  '${}.00'.format(randint(0, 2000)),
                                                                  '${}.00'.format(randint(0, 5000))), commit=True)

    def get_location(self, id):
        return self.execute("""SELECT * FROM gps WHERE id={};""".format(id))

    def ex_1(self):
        return '\n' + str(self.execute("""SELECT * FROM car WHERE color='red' and plate LIKE 'AN%';""", fetchall=True))

    def ex_2(self, inp_date):
        res = self.execute(
            """select date_part('hour', charging_started), count(*) from history
               WHERE CAST(charging_started AS DATE)='{}' group by date_part('hour', charging_started)
               order by date_part('hour', charging_started);""".format(inp_date), fetchall=True)

        output_list = list()
        for item in res:
            output_list.append('{}h-{}h: {}'.format(int(item[0]), int(item[0]) + 1, int(item[1])))

        return '\n' + '\n'.join(output_list)

    def ex_3(self, start_date, end_date):
        res = self.execute("""SELECT (
                                  SELECT COUNT(*)
                                  FROM innomachina.rent
                                  WHERE date_part('hour', order_stamp)
                                  BETWEEN 7 AND 10
                                  AND order_stamp
                                  BETWEEN '{}' AND '{}')
                                  AS Morning,
                                  (
                                    SELECT COUNT(*)
                                    FROM innomachina.rent
                                    WHERE date_part('hour', order_stamp)
                                    BETWEEN 12 AND 14
                                    AND order_stamp
                                    BETWEEN '{}' AND '{}')
                                    AS Afternoon,
                                  (
                                    SELECT COUNT(*)
                                    FROM innomachina.rent
                                    WHERE date_part('hour', order_stamp)
                                    BETWEEN 17 AND 19
                                    AND order_stamp
                                    BETWEEN '{}' AND '{}')
                                    AS Afternoon""".format(start_date, end_date, start_date, end_date, start_date, end_date), fetchall=True)
        return('\nMorning: {} \nAfternoon: {} \nEvening: {}'.format(*res[0]))


    def ex_4(self, user_id):
        res = self.execute("""SELECT COUNT(id) FROM transactions WHERE customer={} AND CAST(stamp AS DATE) > '{}' GROUP BY rent;""".format(user_id, (datetime.now()-dateutil.relativedelta.relativedelta(months=1)).strftime("%Y-%m-%d")), fetchall=True)

        for item in res:
            if item[0] > 1:
                return 'User was charged twice at month: {}'

        return "\nUser wasn't charged twice"

    def ex_5(self, date):
        res_str = '\n'

        res = self.execute(
            """SELECT avg(distance) FROM rent where CAST(delivered_stamp AS DATE)='{}';""".format(date))
        res_str += 'Avg distance: ' + str(res[0])

        res = self.execute(
            """SELECT avg(finished_stamp-delivered_stamp) FROM rent where CAST(delivered_stamp AS DATE)='{}';""".format(
                date))
        res_str += ' | Avg duration: ' + str(res[0])

        return res_str

    def ex_6(self):
        res_str = '\nTop 3 pick-up locations:\n'
        for iter_item in [(7, 10), (12, 14), (17, 19)]:
            res = self.execute("""SELECT arrival_from, count(arrival_from) AS mycount FROM rent
                                    WHERE date_part('hour', order_stamp) BETWEEN {} AND {} GROUP BY arrival_from
                                    ORDER BY mycount DESC limit 3;""".format(iter_item[0], iter_item[1]), fetchall=True)

            for res_item in res:
                res_str += '{}h-{}h: '.format(iter_item[0], iter_item[1]) + str(self.get_location(res_item[0])) + '\n'

        res_str += '\n\nTop 3 travel destinations:\n'
        for iter_item in [(7, 10), (12, 14), (17, 19)]:
            res = self.execute("""SELECT arrived_to, count(arrived_to) AS mycount FROM rent
                                    WHERE date_part('hour', finished_stamp) BETWEEN {} AND {} GROUP BY arrived_to
                                    ORDER BY mycount DESC limit 3;""".format(iter_item[0], iter_item[1]), fetchall=True)

            for res_item in res:
                res_str += '{}h-{}h: '.format(iter_item[0], iter_item[1]) + str(self.get_location(res_item[0])) + '\n'

        return res_str

    def ex_7(self):
        res = self.execute("""SELECT rent.car, COUNT(rent.car) AS cnt FROM rent
                                WHERE order_stamp > CURRENT_DATE - INTERVAL '3 months' GROUP BY rent.car
                                UNION
                                  SELECT car.id, 0 as cnt FROM car
                                EXCEPT
                                  SELECT rent.car, 0 as cnt from rent
                                  WHERE order_stamp > CURRENT_DATE - INTERVAL '3 months' ORDER BY cnt;""",
                             fetchall=True)

        if len(res)//10 < 1:
            res = res[0]
        else:
            res = res[0:len(res) // 10]

        return '\ncar | count\n' + str(res)

    def ex_8(self):
        res = self.execute("""SELECT history.customer, COUNT(*) FROM innomachina.history
                                WHERE date_part('month', history.charging_started) = date_part('month', timestamp '{}')
                                AND history.charging_started <= timestamp '{}' GROUP BY history.customer;""".format((datetime.now()-dateutil.relativedelta.relativedelta(months=2)).strftime("%Y-%m-%d"), datetime.now().strftime("%Y-%m-%d")), fetchall=True)

        return '\nUserId | Amount \n' + '\n'.join([str(x) for x in res])

    def ex_9(self):
        res = self.execute("""SELECT AVG(temp_1.count), temp_1.workshop, temp_1.part, temp_1.p_type FROM (
                                SELECT COUNT(*), repairs.workshop, repair_used_parts.part, part.p_type, date_part('week', repairs.repair_start)
                                FROM innomachina.repairs
                                RIGHT OUTER JOIN innomachina.repair_used_parts ON repairs.id = repair_used_parts.repair
                                RIGHT OUTER JOIN innomachina.part ON repair_used_parts.part = part.id
                                GROUP BY date_part('week', repairs.repair_start), repairs.workshop, repair_used_parts.part, part.p_type
                                ORDER BY COUNT(*) DESC
                                ) temp_1
                                GROUP BY temp_1.workshop, temp_1.part, temp_1.p_type
                                ORDER BY AVG(temp_1.count) DESC;""", fetchall=True)

        return '\navg | workshop | part | part \n' + '\n'.join([str(x) for x in res])

    def ex_10(self):
        res = self.execute("""SELECT a.car, a.sum::NUMERIC + b.sum::NUMERIC AS ord FROM (SELECT DISTINCT car, SUM(price)
                                FROM (history LEFT JOIN plug ON plug.id=history.plug LEFT JOIN station ON plug.station=station.id) GROUP BY car) as a
                                LEFT JOIN (SELECT DISTINCT test2.car, SUM(test2.labour)/COUNT(test1.car) AS sum from repairs AS test1
                                LEFT JOIN (SELECT DISTINCT car, labour FROM repairs) AS test2 ON test1.car=test2.car GROUP BY test2.car) as b
                                ON a.car=b.car GROUP BY a.car, a.sum, b.sum ORDER BY ord DESC LIMIT 1;""")
        return '\ncar | sum_price\n{} {}'.format(res[0], res[1])
