from database.DatabaseWorker import Worker

if __name__ == '__main__':
    worker = Worker('root', 'innomachina', 'password', '167.99.143.25', 5432)
    # worker.add_rent_car(2)
    print('ex_1', worker.ex_1(), '\n')
    print('ex_2', worker.ex_2('2018-10-01'), '\n')
    print('ex_3', worker.ex_3('2008-05-08 00:00:00', '2009-11-15 00:00:00'), '\n')
    print('ex_4', worker.ex_4(user_id=1), '\n')
    print('ex_5', worker.ex_5(date='2018-05-22'), '\n')
    print('ex_6', worker.ex_6(), '\n')
    print('ex_7', worker.ex_7(), '\n')
    print('ex_8', worker.ex_8(), '\n')
    print('ex_9', worker.ex_9(), '\n')
    print('ex_10', worker.ex_10(),'\n')
